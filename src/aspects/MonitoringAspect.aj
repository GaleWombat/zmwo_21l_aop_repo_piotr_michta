package aspects;

import org.aspectj.lang.reflect.CodeSignature;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;

public aspect MonitoringAspect {

    private long startTime;

    public MonitoringAspect() {
        File logFile = new File("logFile.txt");
        try {
            if (logFile.createNewFile()) {
                System.out.println("Created log file\n");
            } else {
                System.out.println("Log file already exist\n");
            }
        } catch (IOException e) {
            System.err.println("An error occurred in document creation.");
            e.printStackTrace();
        }
    }

    pointcut methodCall():
            cflow(call(* repository.*.*(..))) && !within(MonitoringAspect);

    before(): methodCall() {
        startTime = System.currentTimeMillis();
    }

    after() returning(Object returned): methodCall(){
        long endTime = System.currentTimeMillis();
        try {
            FileWriter logFileWriter = new FileWriter("logFile.txt", true);
            StringBuilder logBuilder = new StringBuilder();
            logBuilder.append(Date.from(Instant.now()));
            logBuilder.append(" | Target class: ");
            String className = thisJoinPoint.getSignature().toShortString().split("\\.")[0];
            if (className.endsWith("(")) className = className.substring(0, className.length() - 1);
            logBuilder.append(className);
            logBuilder.append(" | Method name: ");
            logBuilder.append(thisJoinPoint.getSignature().getName());
            logBuilder.append(" | Number of arguments: ");
            logBuilder.append(thisJoinPoint.getArgs().length);
            if (thisJoinPoint.getArgs().length > 0) {
                logBuilder.append(" | Arguments names: ");
                for (var arg : ((CodeSignature) thisJoinPoint.getSignature()).getParameterNames()) {
                    logBuilder.append(arg + " ");
                }
                logBuilder.append(" | Arguments types: ");
                for (var arg : ((CodeSignature) thisJoinPoint.getSignature()).getParameterTypes()) {
                    logBuilder.append(arg + " ");
                }
                logBuilder.append(" | Arguments values: ");
                for (var arg : thisJoinPoint.getArgs()) {
                    logBuilder.append(arg + " ");
                }
            }
            logBuilder.append("| Method result: ");
            logBuilder.append(returned);
            logBuilder.append(" | Method run time: ");
            logBuilder.append(endTime - startTime);
            logBuilder.append("\n");
            logFileWriter.write(logBuilder.toString());
            logFileWriter.close();
        } catch (IOException e) {
            System.err.println("An error occurred in document writing after.");
            e.printStackTrace();
        }
    }

}
