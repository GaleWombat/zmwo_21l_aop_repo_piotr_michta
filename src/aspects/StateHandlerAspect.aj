package aspects;

import repository.Repository;

import java.io.*;

public aspect StateHandlerAspect {

    declare parents: Repository implements Serializable;
    public static Repository Repository.getLastState() {return loadRepository();}

    private final static String serializedRepositoryFileName = "lastRepositoryState";

    pointcut repositoryModification():
            execution(* repository.Repository.add*(..))
                    || execution(* repository.Repository.update*(..))
                    || execution(* repository.Repository.delete*(..));

    after(): repositoryModification(){
        Repository targetRepository = (Repository) thisJoinPoint.getTarget();
        saveRepository(targetRepository);
    }

    private void saveRepository(Repository targetRepository) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream( serializedRepositoryFileName + ".ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(targetRepository);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            System.err.println("Error occurred on saving last repository state");
            e.printStackTrace();
        }
    }

    private static Repository loadRepository() {
        Repository loadedRepository = null;
        try {
            FileInputStream fileInputStream = new FileInputStream("../" + serializedRepositoryFileName + ".ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            loadedRepository = (Repository) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Error occurred on loading last repository state");
            e.printStackTrace();
        }
        return loadedRepository;
    }
}
