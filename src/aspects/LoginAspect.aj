package aspects;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public aspect LoginAspect {

    private boolean isLoggedIn = false;
    private Map<String, String> loginData;

    public LoginAspect(){
        this.loginData = new HashMap<>();
        try {
            File loginData = new File("loginData.txt");
            Scanner scanner = new Scanner(loginData);
            while(scanner.hasNext()){
                String[] line = scanner.nextLine().split(":");
                this.loginData.put(line[0], line[1]);
            }

        } catch (FileNotFoundException e) {
            System.err.println("Error occurred on loginData reading");
        }
    }

    pointcut repositoryModification():
            execution(* repository.Repository.add*(..))
                    || execution(* repository.Repository.update*(..))
                    || execution(* repository.Repository.delete*(..));

    before() : repositoryModification(){
        if(!isLoggedIn){
            System.out.println("Log yourself to modify repository:");
            Scanner scanner = new Scanner(System.in);
            while(true){
                System.out.println("User: ");
                String name = scanner.nextLine();
                System.out.println("Password: ");
                String pass = scanner.nextLine();
                if(loginData.get(name)!=null && loginData.get(name).equals(pass)){
                    System.out.println("Logged as "+name);
                    isLoggedIn = true;
                    break;
                } else {
                    System.out.println("Invalid username or password");
                }
            }
        }
    }
}
