package aspects;

import java.io.IOException;

public aspect IOExceptionSoftenerAspect {
    declare soft: IOException : execution(* repository.Document.setContent(..));

    after() throwing(IOException exception) : handler(exception) {
        System.err.println("Failed to write to file");
    }
}
