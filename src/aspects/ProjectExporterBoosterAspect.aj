package aspects;

import repository.Document;
import repository.Project;
import repository.Task;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public aspect ProjectExporterBoosterAspect {

    private final List<String> projectsNames;
    private final Map<String, Boolean> hasProjectChanged;
    private final Map<String, List<Task>> projectsTasks;
    private final Map<String, List<Document>> projectsDocuments;

    public ProjectExporterBoosterAspect() {
        this.projectsNames = new ArrayList<>();
        this.hasProjectChanged = new HashMap<>();
        this.projectsTasks = new HashMap<>();
        this.projectsDocuments = new HashMap<>();
    }

    pointcut projectDataModification():
            execution(* repository.Project.add*(..))
                    || execution(* repository.Project.update*(..))
                    || execution(* repository.Project.delete*(..));

    pointcut projectExportCall():
            call(void repository.ProjectExporter.export(..));

    after(): projectDataModification(){
        hasProjectChanged.replace(((Project) thisJoinPoint.getTarget()).getName(), true);
    }

    void around(): projectExportCall() {
        String name = ((Project) thisJoinPoint.getArgs()[0]).getName();
        if (hasProjectChanged.get(name) != null && !hasProjectChanged.get(name)) {
            PrintStream printStream = (PrintStream) thisJoinPoint.getArgs()[1];
            printStream.println("Liczba zadań: " + projectsTasks.get(name).size());
            for (Task task : projectsTasks.get(name)) {
                printStream.println("Zadanie: " + task.getId());
                printStream.println("Opis: " + task.getDescription());
                printStream.println("Zgłaszający: " + task.getReporter().getName() + " " + task.getReporter().getLastName());
                printStream.println("Przypisany: " + task.getAssignee().getName() + " " + task.getAssignee().getLastName());
                printStream.println("Status: " + task.getStatus());
            }
            printStream.println("Liczba dokumentów: " + projectsDocuments.get(name).size());
            for (Document document : projectsDocuments.get(name)) {
                printStream.println("Dokument: " + document.getName());
            }
        } else {
            proceed();
        }
    }

    after(): projectExportCall() {
        Project lastProject = (Project) thisJoinPoint.getArgs()[0];
        String name = lastProject.getName();
        if (hasProjectChanged.get(name) == null) {
            projectsNames.add(name);
            hasProjectChanged.put(name, false);
            projectsTasks.put(name, lastProject.getTaks());
            projectsDocuments.put(name, lastProject.getDocuments());
        } else if (hasProjectChanged.get(name)){
            hasProjectChanged.replace(name, false);
            projectsTasks.replace(name, lastProject.getTaks());
            projectsDocuments.replace(name, lastProject.getDocuments());
        }
    }
}
